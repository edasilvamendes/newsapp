import React, { Component } from 'react'
import { View, Text, Image, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { ActivityIndicator } from 'react-native-paper'

class Loading extends Component {
    //DEFINIE LE TYPE DES VARIABLES PROPS
    static propTypes = {
        displayColor: PropTypes.string.isRequired
    }

    state = {}
    
    render() {
        return (
            <View style={{ flex : 1, justifyContent: 'center', alignItems: 'center'}}>
                <ActivityIndicator size='large' color={this.props.displayColor} />
                <Text style={{color: this.props.displayColor}}>Chargement ...</Text>
                {this.props.children}
            </View>
        );
    }
}

export default Loading