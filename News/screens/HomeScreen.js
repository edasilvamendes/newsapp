import React, { Component } from 'react';
import { Button, Text, Alert, View, ActivityIndicator, FlatList } from 'react-native';
import { Container, Content, List } from 'native-base';
import { Image } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';

import { getArticles } from '../service/NewService';
import NewService from '../service/NewService';

export default class HomeScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            //isLoading: true,
            data: null,
            title: ''
        }
    }

    componentDidMount() {
        getArticles().then(data => {
            this.setState({
                //isLoading: false,
                data: data
            });
        }, error => {
            Alert.alert('Error', 'Something went wrong!');
            }
        )
    }

    render() {
        const { navigate } = this.props.navigation;
        console.log(this.state.data);
        return (

            <List
                dataArray={this.state.data}
                renderRow={(item) => {
                return (
                    <View>
                        <Text key={item.title} style={{marginBottom: 10}}>Titre: {item.title}</Text>
                        <Text key={item.source.name} style={{marginBottom: 10}}>Source: {item.source.name}</Text>
                        <Text key={item.publishedAt} style={{marginBottom: 10}}>Date de publication: {item.publishedAt}</Text>
                        <Image source={{ uri: item.urlToImage }} style={{ width: 250, height: 250, marginBottom: 10 }} />
                    
                        <Button style={{ marginBottom: 50 }}
                            title="DETAILS"
                            onPress={() => navigate('Details', {JSON_ListView_Clicked_Item: item})}      
                        />
                    </View>
                )
            }} />
            
        )
    }

}