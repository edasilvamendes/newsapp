import React, { Component } from 'react'
import { View, Text, Button } from 'react-native';
import { containerStyle } from '../assets/globalStyles';

export default class SettingsScreen extends Component {
    
    state = {
        category: ''
    }

    render() {
        return (
            <View style={containerStyle.container}>
                <Text>CATEGOY</Text>

                <View style={{ flex: 1 }}>
                    <Button title="business" onPress={() => this.setState({category: "business"})} />
                    <Button title="general" onPress={() => this.setState({category: "sports"})}  />
                </View>
            </View>
        )
    }
}
