import React, { Component } from 'react'
import { StyleSheet, Button, View, Text } from 'react-native';
import { containerStyle } from '../assets/globalStyles';
import { Container, Content, List } from 'native-base';
import { Image } from 'react-native-elements';

export default class DetailsScreen extends Component {

    state = {
        title: ''
    }

    back = () => {
        this.props.navigation.goBack();
    }

    render() {
        const { navigate } = this.props.navigation;
        const item = this.props.navigation.state.params.JSON_ListView_Clicked_Item;

        return (
            <View style={containerStyle.container}>
                <Button title="Back" 
                    onPress={this.back}
                />

                <Text key={item.title} style={{marginBottom: 10}}>Titre: {item.title}</Text>
                <Image source={{ uri: item.urlToImage }} style={{ width: 250, height: 250, marginBottom: 10 }} />
                <Text key={item.publishedAt} style={{marginBottom: 10}}>Date de publication: {item.publishedAt}</Text>
                <Text key={item.source.name} style={{marginBottom: 10}}>Source: {item.author}</Text>
                <Text key={item.source.content} style={{marginBottom: 10}}>Contenu: {item.content}</Text>
            </View>
        )
    }
}
