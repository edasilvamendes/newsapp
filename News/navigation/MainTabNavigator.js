import React from 'react'

import HomeScreen from '../screens/HomeScreen';
import DetailsScreen from '../screens/DetailsScreen';
import SettingsScreen from '../screens/SettingsScreen';

import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
import Icon from 'react-native-vector-icons/Ionicons';

const tabNavigator = createMaterialBottomTabNavigator({
    
    Home: {
        screen: HomeScreen,
        navigationOptions:{
            tabBarLabel: 'Accueil',
            tabBarIcon: ({tintColor}) => (
                <Icon color={tintColor} size={25} name={'ios-home'}/>
            ),
            barStyle: {backgroundColor: 'red'}
        }
    },
    Details: {
        screen: DetailsScreen,
        navigationOptions:{
            tabBarLabel: 'DETAILS ARTICLES',
            barStyle: {backgroundColor: 'blue'}
        }
    },
    Settings: {
        screen: SettingsScreen,
        navigationOptions:{
            tabBarLabel: 'Paramètres',
            tabBarIcon: ({tintColor}) => (
                <Icon color={tintColor} size={25} name={'ios-settings'}/>
            ),
            barStyle: {backgroundColor: 'blue'}
        }
    }},
    { 
        initialRouteName:'Home',
        shifting:true
    }
) 

export default createAppContainer(tabNavigator)