export const articles_url = 'https://newsapi.org/v2/top-headlines';
export const country_code = 'fr';
export const category = 'general';
export const _api_key = '9cb9d883e8004f08a8e18daf96a0039c';

export async function getArticles(category='general') {

    try {
        let articles = await fetch(`${articles_url}?country=${country_code}&category=${category}`, {
            headers: {
                'X-API-KEY': _api_key
            }
        });

        let result = await articles.json();
        articles = null;

        return result.articles;
    }
    catch(error) {
        throw error;
    }
}


export async function getArticlesByCateogory(userChoiceCategory) {
    try {
        let articles = await fetch(`${articles_url}?country=${country_code}&category=${userChoiceCategory}`, {
            headers: {
                'X-API-KEY': _api_key
            }
        });

        let result = await articles.json();
        articles = null;

        return result.articles;
    }
    catch(error) {
        throw error;
    }
}